import Glide from "@glidejs/glide"
import "@glidejs/glide/dist/css/glide.core.css";
import "@glidejs/glide/dist/css/glide.theme.css";

import { Controls } from '@glidejs/glide/dist/glide.modular.esm'

new Glide('.glide', {
  type: 'carousel',
  startAt: 0,
  perView: 2.55
}).mount({ Controls })
