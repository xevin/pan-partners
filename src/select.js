import { $ } from "./jq.js"
import Choices from "choices.js"
import "choices.js/public/assets/styles/choices.css"


new Choices($("#select-city"), {
  searchEnabled: false,
  noChoicesText: 'Выбрать город',
  itemSelectText: ''
});
