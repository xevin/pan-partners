import { $, $$ } from "./jq.js"
import "./select.js"
import "./slider.js"

console.info("Battlecruiser operational");

$(".menu__item a").addEventListener("click", (el) => {
  el.target.classList.toggle("active");
  const $submenu = el.target.parentNode.querySelector(".submenu")
  $submenu.classList.toggle("active")
})

$$(".submenu__item").forEach(el => {
  el.addEventListener("click", (ev) => {
    ev.stopPropagation()
    el.parentNode.classList.remove("active")
    $(".menu__item a.active").classList.remove("active")
  })
})

$$(".form").forEach((el) => {
  el.addEventListener("submit", e => {
    e.preventDefault();
  })
})

$(".activate-progress-bar").addEventListener("click", (e) => {
  const $targetBar = $(e.target.dataset.target)

  const addPercent = e.target.dataset.add
  const currentWidth = $targetBar.style.width.replace("%", "");
  const newWidth = (Number(currentWidth) + Number(addPercent)) > 100
    ? 100
    : Number(currentWidth) + Number(addPercent)
  $targetBar.style.width = `${newWidth}%`
  $targetBar.innerText = `${newWidth}%`
});

$$(".spoiler").forEach((el) => {
  el.addEventListener("click", () => {
    $(el.dataset.target).classList.toggle("hidden-by-spoiler")
    el.classList.toggle("active")
  })
})